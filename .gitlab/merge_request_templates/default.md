# Tasks

## General

- [ ] [Release Notes](https://gitlab.com/flywheel-io/flywheel-apps/templates/skeleton/-/blob/main/CONTRIBUTING.md#populating-release-notes)

- [ ] [Changelog](https://gitlab.com/flywheel-io/flywheel-apps/templates/skeleton/-/blob/main/CONTRIBUTING.md#adding-changelog-entry)

- [ ] Tests

## Gear specific (if applicable)

- [ ] Example run:

- [ ] Regression test for gear added to Qase
