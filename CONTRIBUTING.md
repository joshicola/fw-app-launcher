# Contributing

## Getting started

1. Follow instructions to [install poetry](https://python-poetry.org/docs/#installation).
2. Follow instructions to [install pre-commit](https://pre-commit.com/#install)

After cloning the repo:

1. `poetry install`: Install project and all dependencies
   (see **Dependency management** below)
2. `pre-commit install`: Install pre-commit hooks (see **Linting and Testing** below)

## Dependency management

This gear uses [`poetry`](https://python-poetry.org/) to manage dependencies,
develop, build and publish.

### Dependencies

Dependencies are listed in the `pyproject.toml` file.

#### Managing dependencies

- Adding: Use `poetry add [--dev] <dep>`
- Removing: Use `poetry remove [--dev] <dep>`
- Updating: Use `poetry update <dep>` or `poetry update` to update all deps.
  - Can also not update development dependencies with `--no-dev`
  - Update dry run: `--dry-run`

#### Using a different version of python

Poetry manages virtual environments and can create a virtual environment with different
versions of python, however that version must be installed on the machine.

You can configure the python version by using `poetry env use <path/to/executable>`

#### Helpful poetry config options

See full options
[Here](https://python-poetry.org/docs/configuration/#available-settings).

List current config: `poetry config --listn`

- `poetry config virtualenvs.in-project <true|false|None>`: create virtual environment
  inside project directory
- `poetry config virtualenvs.path <path>`: Path to virtual environment directory.

## Linting and Testing

Local linting and testing scripts are managed through
[`pre-commit`](https://pre-commit.com/).  
Pre-commit allows running hooks which can be defined locally, or in other
repositories. Default hooks to run on each commit:

- check-json: JSON syntax validator
- check-toml: TOML syntax validator (for pyproject.toml)
- pretty-format-json: Pretty print json
- no-commit-to-branch: Don't allow committing to master
- isort-poetry: Run isort in poetry venv
- black-poetry: Run black in poetry venv
- pytest-poetry: Run pytest in poetry venv

These hooks will all run automatically on commit, but can also be run manually
or just be disabled.

### pre-commit usage

- Run hooks manually:
  - Run on all files: `pre-commit run -a`
  - Run on certain files: `pre-commit run --files test/*`
- Update (e.g. clean and install) hooks: `pre-commit clean && pre-commit install`
- Disable all hooks: `pre-commit uninstall`
- Enable all hooks: `pre-commit install`
- Skip a hook on commit: `SKIP=<hook-name> git commit`
- Skip all hooks on commit: `git commit --no-verify`

## Adding a contribution

Every contribution should be associated with a ticket on the GEAR JIRA board, or be a
hotfix. You should contribute by creating a branch titled with either
`hotfix-<hotfix_name` or `GEAR-<gear_num>-<description>`. For now, other branch names
will be accepted, but soon branch names will be rejected if they don't follow this
pattern.

When contributing, make a Merge Request against the main branch.

### Merge requests

The merge request should contain at least two things:

1. Your relevant change
2. Update the corresponding entry under `docs/release_notes.md`

Adding the release notes does two things:

1. It makes it easier for the reviewer to identify what relevant changes they should
   expect and look for in the MR, and
2. It makes it easier to create a release./

#### Populating release notes

For example, if the gear is currently on version `0.2.1` and you are working on a
bugfix under the branch GEAR-999-my-bugfix. When you create a merge request against
`main`, you should add a section to `docs/release_notes.md` such as the following:

```markdown
## 0.2.2

BUG:

- Fixed my-bug, see [GEAR-999](https://flywheelio.atlassian.net/browse/GEAR-999)
```

Where the rest of the file contains release notes for previous versions.

#### Adding changelog entry

The [changelog](./docs/changelog.md) is a place to put more informal notes about large
design decisions. This is useful to look back on design decisions made by you, or
other engineers and try to understand why. This is not required, but is encouraged for
large changes.

### Creating a release

When your merge request is reviewed and approved, you should pull from main locally:

```bash
git checkout main # Locally change to main branch
git pull origin main # Locally pull updates from main branch
```

Then update the versions accordingly:

1. Update poetry version: `poetry version <new_version`
2. Update the version in the manifest:
   1. Update `"version"` key with new version
   2. Update `"custom.flywheel.gear-builder.image"` key with new image version
3. Commit version changes

Then you can tag the version and push:

```bash
git tag <new_version>
git push origin main
git push origin --tags
```

Once you've pushed tags, you can go to the gitlab UI -> Project Overview -> Releases
and create a new release. You can copy the release notes that are already populated in
the `docs/release_notes.md` document.

## Development Notes

These are notes to assist in developing and maintaining the `fw-app-launcher` in the
absence of the original developer.

This will not be a complete introduction to GUI programming in Python or the myriad of
other options to do so (Pyside, tktl, etc.). This document aims, instead, to quickly
direct the developer/maintainer to resources that most readily facilitate that
development.

## PyQt6

The `requirements.txt` of this repository contains the version of tools that this
application is built upon. However, installation of the GUI Designer (**QT Designer**)
is not included in these resources for Mac OS X.

### PyQt6 Designer

Multiple resources exist for installing the PyQt6 Designer on various platforms:

- linux: <https://gist.github.com/ujjwal96/1dcd57542bdaf3c9d1b0dd526ccd44ff>
- os x:
  <https://stackoverflow.com/questions/46542326/installing-pyqt5-incl-qt-designer-on-macos-sierra>
- windows: <https://build-system.fman.io/qt-designer-download>

### Simple Example

With a PyQt6 Form designed and ready for execution, it is important to have the
functional elements perform actions. This is going to occur from "binding" events and
signals to the functions that will perform the desired behavior.

```python
cbo_type.currentIndexChanged.connect(config_type_changed)
```

The above code connects the `currentIndexChanged` event of the combo box `cbo_type` to
a function `config_type_changed`.

## Code Structure

The project is divided into various sections to assist with ease of use.

### Top-Level

On the top-level, there is the `fw_app_launcher.py`. This is contains the central
application object and reference to the main form. It is intended to be a brief
reference to the three groups of related functionality. These three groups are: The
tree representation of the Flywheel Container Hierarchy, the list of available
applications to view data in, and the local analyses that we are saving data to.

As with the other form components, the `.ui` file that defines the form and all the
tabs resides in `./resources/`. We will revisit this later.

### resources

The `./resources` directory contains all the functionality and resources for the gear
components. This includes images for the container tree and forms (.ui) for rendering.

### management

The `./management` directory contains the python files that are responsible for
managing each of class of functionality.

## TODOs

- [ ] Move "Node ID" functionality to a context menu item in the TreeManagement class
- [ ] Flywheel logo and the instance they are logged into
- [ ] Recursively cache selected acquisitions.
- [ ] File download progress bar.
- [ ] BrainLife Example: [https__://youtu.be/H21eKZDJYxg?t=45]
- [ ] Analyses need to be associated with an instance...regardless of their user
      permissions.
