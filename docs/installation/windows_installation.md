# Windows Installation

## Download

Download the latest version of the app from the [releases
page](https://drive.google.com/drive/u/1/folders/1iDlJh2BxDZ9JfUwwcc2U3wipBzuMNPfM).

## Install

By running the installer, the application will be installed to the `C:\Program
Files\Flywheel\Flywheel App Launcher` folder.

### Windows Requirements

Flywheel Application Launcher requires Windows 10 or above to either have
[Developer Mode](https://consumer.huawei.com/en/support/content/en-us15594140/)
enabled or to run 3D Slicer
[As
Administrator](<https://www.computerhope.com/issues/ch001197.htm#:~:text=To%20run%20a%20p>
rogram%20as%20Administrator%20in%20Windows%2010%2C%20right,and%20select%20Run%20as%20adm
inistrator.).

This is to support the directory structure with symbolic links to files.
