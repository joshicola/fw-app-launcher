@ECHO OFF

IF EXIST "fw_app_launcher.spec" (
    ECHO "poetry run pyinstaller --noconfirm fw_app_launcher.spec "
    poetry run pyinstaller --noconfirm fw_app_launcher.spec 
) ELSE (
    ECHO "poetry run pyinstaller (first time)"
    poetry run pyinstaller ^
        -y ^
        --windowed ^
        --icon resources/flywheel.ico ^
        --add-data apps;apps ^
        --add-data resources;resources ^
        --add-data pyproject.toml;. ^
        --collect-all flywheel_gear_toolkit ^
        --collect-all fw_meta ^
        --collect-all fw_utils ^
        --collect-all fw_client ^
        --collect-all fw_http_client ^
        --collect-all nibabel ^
        --upx-dir C:\Users\joshi\Projects\fw_projects\Downloads ^
        fw_app_launcher.py
)

@REM poetry run python bin/create_installer_name.py -i ../Flywheel_App_Launcher_Install.exe -t pyproject.toml -o windows -a amd64 -e exe