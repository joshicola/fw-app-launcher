"""__summary__."""

import logging
import os
from pathlib import Path

log = logging.getLogger(__name__)


def start(applauncher, keyvals=None):
    """Do anything that needs to be done when this use case is started.

    This replaces the start() function in ITK-SNAP/run.py for this use case.

    Args:
        applauncher (QApplication): _description_
        keyvals (dict): _description_. Defaults to None.

    Returns:
        bool: True if successful, False otherwise
    """
    if not keyvals:
        return False

    app_name = applauncher.ui.AppName.text()
    log.debug("Starting %s %s", app_name, applauncher.app_management.use_script)

    # Get group and project from destination container and override what was read
    # from ~/.config/flywheel/fw_app_launcher.json
    fw = applauncher.fw_client
    # Check that the destination is at the host and has permissions.
    try:
        file_parent = fw.get(keyvals["destination"])
        file_name = keyvals["file_name"]
        applauncher.group = fw.get(file_parent.parents.group)
        applauncher.project = fw.get(file_parent.parents.project)
    except Exception as e:
        log.error("Could not get destination %s.", keyvals["destination"])
        log.error("Please check that you are logged into the correct Flywheel instance")
        log.error(" and that you have permissions to access the destination.")

        return False

    # first set destination (initiate local "analysis" and set its destination)
    applauncher.analysis_management.set_destination(keyvals["destination"])

    file_entry = None
    for file in file_parent.files:
        if file_name == file.name:
            file_entry = file

    if file_entry:
        applauncher.analysis_management.set_input("g", file_entry)
    else:
        log.error("Could not find %s", file_name)

    # Download file
    full_input_dir = applauncher.analysis_management.analysis_dir / "input" / "g"
    full_input_dir.mkdir(parents=True, exist_ok=True)
    file_path = full_input_dir / file_name
    file_parent.download_file(file_name, str(file_path))
    log.info("downloaded %s", file_name)

    return True
    # Here, set any applauncher.analysis_management.gear_config["config"][key] from what is in
    # applauncher.configs to their desired value (possibly provided in the URL)
    # Then
    #    with open(applauncher.analysis_management.config_file, "w") as fp:
    #        json.dump(applauncher.analysis_management.gear_config, fp, indent=4)


def get_file_mod_times(path):
    """Return a dictionary of time stamps for all files in the path.

    Args:
        path (Path): path to directory where files might be

    Returns:
        dictionary: modification times for each file. Keys are full file paths
    """
    file_mod_times = dict()
    len_path = len(str(path)) + 1
    for dp, dn, filenames in os.walk(path):
        for f in filenames:
            full_path = Path(dp) / f
            key = str(full_path)[len_path:]
            file_mod_times[key] = os.path.getmtime(full_path)
    return file_mod_times


def before(application):
    """Do anything that needs to be done before running the app.

    Args:
        application (fw-app-launcher PyQt app): the calling application
    """
    app_name = application.ui.AppName.text()
    log.debug("Before %s %s", app_name, application.app_management.use_script)

    # list the files that are present in the analysis to compare with what is there afterwards
    file_mod_times = get_file_mod_times(application.analysis_management.analysis_dir)
    application.analysis_management.file_mod_times = file_mod_times


def after(application):
    """Do anything that needs to be done after running the app.

    Args:
        application (fw-app-launcher PyQt app): the calling application
    """
    app_name = application.ui.AppName.text()
    log.debug("After %s %s", app_name, application.app_management.use_script)

    # list the files in the analysis and move any files that were created or update to the output
    old_mod_times = application.analysis_management.file_mod_times
    new_mod_times = get_file_mod_times(application.analysis_management.analysis_dir)

    files_to_put_in_output = list()
    for key in new_mod_times:
        if key in old_mod_times:
            if old_mod_times[key] != new_mod_times[key]:
                files_to_put_in_output.append(key)
        else:
            files_to_put_in_output.append(key)

    for ff in files_to_put_in_output:
        changed_file_name = Path(ff).name
        old_place = application.analysis_management.analysis_dir / ff
        new_place = (
            application.analysis_management.analysis_dir / "output" / changed_file_name
        )
        os.rename(old_place, new_place)


def stop(app_management):
    """Do anything that needs to be done before quitting this use case.

    Args:
        app_management (_type_): _description_
    """
    app_name = app_management.main_window.ui.AppName.text()
    log.debug("Stopping %s %s", app_name, app_management.use_script)
