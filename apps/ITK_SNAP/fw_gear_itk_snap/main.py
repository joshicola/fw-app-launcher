"""Main module to run the local application."""

import logging

from flywheel_gear_toolkit.interfaces.command_line import exec_command

log = logging.getLogger(__name__)


def run(command, params):
    """Use python's subprocess to launch a local application using a command line.

    Returns:
        Output from the local application and exit status.
    """
    log.info("This is the beginning of the run file")

    command += params

    stdout, stderr, returncode = exec_command(command)

    if stdout != "":
        log.info(stdout)

    if stderr != "":
        log.error(stderr)

    return stdout, stderr, returncode
