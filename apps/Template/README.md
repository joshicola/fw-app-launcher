# Installing your own local app

To create your own app that can be called by the fw_app_launcher, clone
<https://gitlab.com/flywheel-io/scientific-solutions/app/fw-app-launcher> and take a
look at the code and documentation (*.py and*.html) in the `apps/` directory.  You'll
need to copy this Template into your own "apps" directory.  You determine where that is
by setting the value of "Apps Path".  You can do this the very first time you run the
fw_app_launcher or by pressing the "Settings" button in the fw_app_launcher.  You can
use a full path (starting with "/") or else use something like "\~/my-app-dir" (where
"\~" will resolve into your home directory).  Then, from the fw_app_launcher main
directory, you can use

```shell
cp -r apps/Template ~/my-app-dir/My_New_App
```

to start to set up your own local application.  Don't put spaces or dashes ("-") in the
name because this directory and its subdirectories will need to be loaded as part of
the path to python modules so that the code you write can be called.  Also, don't use
any existing name ("ITK_SNAP" or "Template") because the fw_app_launcher will present
you with a list of apps that you can launch and they must be unique.

Next, you'll want to edit the "glue" code to make your application and use case(s)
actually work.  Each directory in the `apps/` directory has a structure similar to a
real Flywheel gear:

* `fw_template/`   - a directory that contains code that is common to all use cases.
It is usually called` "fw_\<your app library name\>".
* `manifest.json`  - a gear manifest file specifying inputs, configuration parameters,
and information about the gear
* `run.py`         - the main code to run the gear including `start()` `main()`, and
`stop()`.
* `uses/`          - directories for each use case
* `__init.py__`    - a file that allows `run.py` to be loaded as a module.  It can be
empty.

The `start()` code in `run.py` is called when the apps glue code is first loaded as a
module.  It can be used to do any initialization that is necessary for all use cases.
The `main()` function is called when the fw_app_launcher has everything ready to run
the local application.  You probably don't need to modify it.  It calls
`parse_config()` and `run()` which are defined in the `fw_template/` directory in
`parse.py` and `main.py`.  You probably don't need to modify those either because
everything they need is set up by fw_app_launcher.  To make your local application able
to be run under macOS, Linux, and Windows, you do need to put different commands for
each OS in the manifest.json file.  The `stop()` function in `run.py` will be called as
the last thing before quitting the fw_app_launcher.

Use cases are in directories in the `uses/` directory which are structured like

```html
apps/Template/uses/Some_use
├── html
│   ├── 1.html
│   └── 2.html
├── index.html
└── run.py
```

To make clear what your app does, Edit the html files to explain to your users exactly
what they need to do when running the local application for each specific use case.
They have to do the right thing for the glue code to be able to access the desired
input file(s) and upload the output(s).  The `uses/` directory has a file called
index.html that points to each use case.

The main "glue" code you will need to edit is in, e.g.
`apps/Template/uses/Some_use/run.py`.  The functions are:

* `start()`   -> run when the use case module is first loaded
* `before()`  -> run just before the local application is called (in
`apps/Template/run.py`: `main()`)
* `after()`   -> run just after the local application is called and before the results
are uploaded to Flywheel
* `stop()`    -> run just before quitting fw_app_launcher (and before running the above
`apps/Template/run.py` `stop()` function)

Take a look at how things are done in `apps/ITK_SNAP/uses/Segmentation/run.py`.  There,
`start()` and `stop()` don't really do anything.  The important code is in `before()`
and `after()`.

Before the local app can run, code in fw_app_launcher will create a directory to hold
the new Flywheel analysis. This directory will have a `config.json` file and `input/`
and `output/` directories like a real gear being executed on Flywheel.  For each input,
the input name defined in the manifest will be a directory name in `input/` and
fw_app_launcher will download the selected file there.

To figure out what and where the outputs created by ITK-SNAP are, the `before()` code
looks at the time stamp for every file in the analysis directory, including files in
`input/` and `output/`.  Next, the local application is run and then the `after()` code
gets those time stamps again and considers any modified or new file to be a new output
file that needs to be copied into the `output/` directory.  No analysis will be
uploaded to Flywheel if there are no output files.  The code in `after()` also handles
a current issue with the Flywheel platform: output files cannot have the same name as
input files.  If this is detected, the user is asked to provide a new name for the
output file by using a pyqt dialog.

The code in `apps/Template/uses/URL/run.py` is different from the locally-launched use
cases because it has additional information provided by the URL that is generated by
the Flywheel Application (extension SDK) when using the kebab menu.  The magic here is
in the `start()` code.  This use case example grabs only the single specified file
(where the kebab menu is found) and provides it as the "-g" command-line input to
ITK-SNAP.  The code here could be modified to find other files in the same acquisition
container and provide them to ITK-SNAP with the "-o" optional input(s).  This `start()`
code also overrides the group and project that might be specified in the
fw_app_launcher's config file.  That information is found by looking at the destination
container that is obtained from the container ID that is provided in the URL (along
with the file name).  When a URL is detected by fw_app_launcher, it provides the
`start()` function with a dictionary of keywords and values parsed from the URL.  The
current example registered Flywheel Application sets the URL to be like:

```html
fwlaunch://itk_snap?use=URL&destination=639266a18cdd05613d904114&file_name=T1_SAG_SE.nii.gz
```

where

* `itk_snap` is the app found in `apps/` directory.  It is always lower case here but
the app in `apps/` can have upper and lower case letters
* `use=URL` is they key-value pair that is the use case
* `destination=<ID>` provides the ID of the destination container, and
* `file_name=<file name>` provides the name of the file.

The URL can be changed in the Flywheel Application to run a new use case or provide
configuration parameters that will have to be interpreted properly by the `start()`
code.  The URL can also get additional information as specified in the Extension SDK.
