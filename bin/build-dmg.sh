#!/bin/sh
# If the DMG already exists, delete it.
test -f "dist/fw_app_launcher.dmg" && rm "dist/fw_app_launcher.dmg"
create-dmg \
  --volname "fw_app_launcher" \
  --volicon "dist/fw_app_launcher.app/Contents/Resources/resources/Flywheel.icns" \
  --window-pos 200 120 \
  --window-size 600 300 \
  --icon-size 100 \
  --icon "fw_app_launcher.app" 175 120 \
  --hide-extension "fw_app_launcher.app" \
  --app-drop-link 425 120 \
  --codesign "Developer ID Application: Flywheel Exchange, LLC (8BJHB3C2GB)" \
  --notarize "AC_PASSWORD" \
  "dist/fw_app_launcher.dmg" \
  "dist/fw_app_launcher.app"

# Rename with version number
poetry run python bin/create_installer_name.py \
    -i dist/fw_app_launcher.dmg \
    -t ./pyproject.toml \
    -o macosx \
    -a amd64 \
    -e dmg